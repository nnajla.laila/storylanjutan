$(document).ready(function() {
  $("#books").on("keyup", function(e) {
      q = e.currentTarget.value.toLowerCase()
      console.log(q)

      $.ajax({
          url: "data/?q=" + q,
          datatype: 'json',
          success: function(data) {
              $('#result').html('')
              var url = "";
              var img = "";
              var title = "";
              var author = "";
              var like = "";
             
              for (var i = 0; i < data.items.length; i++) {
                title=$('<h5 class="text-center mt-5">' + data.items[i].volumeInfo.title + '</h5>');  
                author=$('<h5 class="text-center"> By: ' + data.items[i].volumeInfo.authors + '</h5>');
                img = $('<img class="img-fluid mx-auto"><br><h5 class="text-center"><a href=' + data.items[i].volumeInfo.infoLink + '><button type="button" class="btn btn-light" style="margin: 0 2% 0 2%">Read More</button></h5>');
                url= data.items[i].volumeInfo.imageLinks.thumbnail;
                img.attr('src', url);
                title.appendTo('#result');
                author.appendTo('#result');
                img.appendTo('#result');

              }
              
          }
      })
  });
});
