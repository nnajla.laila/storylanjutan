from django.test import TestCase
from django.test import TestCase, Client
from django.urls import resolve
from django.http import HttpRequest
from selenium import webdriver
from .apps import Story9Config
from .views import logIn, signUp, logOut
from selenium.webdriver.chrome.options import Options
import os

# Create your tests here.
class UnitTestonStory9(TestCase):

    #LOGIN PAGE TEST
    def test_does_login_page_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)
    
    def test_check_function_used_by_login(self):
        found = resolve('/')
        self.assertEqual(found.func, logIn)

    def test_does_login_views_show_correct_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'login.html')
    
    def test_check_login_page_have_form(self):
        request = HttpRequest()
        response = logIn(request)
        html_response = response.content.decode('utf8')
        self.assertIn('<form', html_response)
    
    def test_signin_header(self):
        request = HttpRequest()
        response = logIn(request)
        html_response = response.content.decode('utf8')
        self.assertContains(response, "Sign in")

    #SIGN UP PAGE TEST
    def test_does_login_page_exist(self):
        response = Client().get('/signup/')
        self.assertEqual(response.status_code, 200)
    
    def test_check_function_used_by_login(self):
        found = resolve('/signup/')
        self.assertEqual(found.func, signUp)
    
    def test_signup_header(self):
        response = Client().get('/signup/')
        self.assertContains(response, "Create a new account")

    def test_does_signup_views_show_correct_template(self):
        response = Client().get('/signup/')
        self.assertTemplateUsed(response, 'signup.html')
    
    #SIGN OUT TEST
    def test_does_sign_out_work(self):
        response = Client().get('/logout/')
        self.assertEqual(response.status_code, 302)

    


class TestApp(TestCase):
	def test_app(self):
		self.assertEqual(Story9Config.name, "story9")
