from django.test import TestCase
from django.test import Client
from django.urls import resolve
from . import views



# Create your tests here.

class Story7UnitTest(TestCase):

    def test_urls_exist(self):
        response = Client().get('/story7/')
        self.assertEqual(response.status_code, 200)

    
    def test_template_used(self):
        response = Client().get('/story7/')
        self.assertTemplateUsed(response, 'index.html')

    def test_func_page(self):
        found = resolve('/story7/')
        self.assertEqual(found.func, views.index)


