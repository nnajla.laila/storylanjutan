from django.apps import AppConfig


class StorykuConfig(AppConfig):
    name = 'storyku'
