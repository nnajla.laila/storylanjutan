from django.urls import path, include
from . import views

app_name = 'storyku'

urlpatterns = [
    path('', views.index, name='index'),
]